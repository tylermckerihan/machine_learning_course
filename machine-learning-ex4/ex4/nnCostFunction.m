function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

% Create y matrix with decoded labels
y_matrix = [];
for label = 1:num_labels
	y_matrix(label,:) = (y==label);
endfor

% ---- Part 1 ---- %
% Theta1 Hypothesis
X = [ones(size(X,1), 1) X];
z1 = X * Theta1';
a2 = sigmoid(z1);

% Theta2 Hypothesis
a2 = [ones(size(a2,1), 1) a2];
z2 = a2 * Theta2';
a3 = sigmoid(z2);

% Non-regularised cost function
logError = -1*y_matrix.*log(a3') - (1-y_matrix).*log(1-a3');
nonReg = (1/m) * sum(sum(logError));

% Negate first column from regularisation
RegTheta1 = Theta1;
RegTheta1(:, 1) = 0;

RegTheta2 = Theta2;
RegTheta2(:, 1) = 0;

% Create regularisation figure
reg = (lambda/(2*m)) * ((sum(sum(RegTheta1.^2))) + sum(sum(RegTheta2.^2)));

% Calculate cost
J = nonReg + reg;

% ---- Part 2 ---- %
% Set all Delta to zero
deltaOutErr = zeros(num_labels, 1);

% Begin Backpropagation loop
for trainingItem = 1:m

    % Begin forward propagation
    % Second activation layer
    a1 = X(trainingItem, :);
    z1 = Theta1 * a1';
    a2 = sigmoid(z1);
    % Last activation layer
    a2 = [1; a2];
    z2 = Theta2 * a2;
    a3 = sigmoid(z2);

    % Begin backwards propagation
    a3Err = a3-y_matrix(:, trainingItem);

    % Add back bias node
    z1 = [1; z1];
    a2Err = (Theta2'*a3Err).*sigmoidGradient(z1);

    % Remove resulting bias node
    a2Err = a2Err(2:end);

    % Calculate gradients
    Theta2_grad = (Theta2_grad + a3Err * a2');
    Theta1_grad = (Theta1_grad + a2Err * a1);
endfor

% % Non-regularisation
% Theta2_grad = Theta2_grad ./ m;
% Theta1_grad = Theta1_grad ./ m;

%Regularise gradients
% Regularise for first layer
Theta1_grad(:,1) = Theta1_grad(:,1)./m;
Theta2_grad(:,1) = Theta2_grad(:,1)./m;

% Regularise for non-first layers
Theta1_grad(:,2:end) = Theta1_grad(:,2:end)./m + ( (lambda/m) * Theta1(:,2:end) );
Theta2_grad(:,2:end) = Theta2_grad(:,2:end)./m + ( (lambda/m) * Theta2(:,2:end) );

% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
